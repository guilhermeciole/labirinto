const botao = document.querySelector(".botao");
const modal = document.querySelector(".modal");

botao.addEventListener("click", function () {
    modal.classList.add("hidden");
    location.reload();
})


const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW"
];

const jogador = document.createElement("div");

for (let i = 0; i < map.length; i++) {

    const linha = document.createElement("div");
    linha.classList.add("linha");
    document.body.appendChild(linha);

    for (let j = 0; j < map[i].length; j++) {

        if (map[i][j] === "W") {
            const parede = document.createElement("div");
            parede.classList.add("parede");
            linha.appendChild(parede);
        }
        else {
            const espaço = document.createElement("div");
            espaço.classList.add("espaço");
            linha.appendChild(espaço);

            if (map[i][j] === "S") {
                jogador.classList.add("jog");
                espaço.appendChild(jogador);
            }
        }
    }
}

let mapa = document.querySelectorAll(".linha>div");
let posicao = 198

document.addEventListener("keydown", function (e) {

    const keyName = e.key;

    if (keyName === "ArrowRight") {
        if (jogador.parentElement.nextElementSibling.className === "espaço") {
            jogador.parentElement.nextElementSibling.appendChild(jogador);
            posicao += 1

            if (posicao === 197) {
                modal.classList.remove("hidden")

            }
        }
    }
    if (keyName === "ArrowLeft") {
        if (jogador.parentElement.previousElementSibling.className === "espaço") {
            posicao -= 1
            jogador.parentElement.previousElementSibling.appendChild(jogador);
        }

    }
    if (keyName === "ArrowUp") {
        if (mapa[posicao - 30].className === "espaço") {
            mapa[posicao - 30].append(jogador);
            posicao -= 21
        }

    }
    if (keyName === "ArrowDown") {
        if (mapa[posicao + 12].className === "espaço") {
            mapa[posicao + 12].append(jogador);
            posicao += 21
        }
    }

})

